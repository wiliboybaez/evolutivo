import React,{Fragment} from 'react';
import {Link} from 'react-router-dom';
import FormTicket from './FormTicket';


const Principal = () => {
    return (  
        <Fragment>
            <div className="contenedor-app">
                <div className="seccion-principal">
                <header className="app-header">
                    <nav className="nav-principal-center">
                    MANAGEMENT OF FLIGHT
                    </nav>
                    <Link to = {'/login'} >
                        <button
                        type="submit"
                        className="btn btn-secundario block"
                        > 
                            SUPERVISOR
                        </button>
                    </Link>
                </header>
                <div className="seccion-principal">
                    <div className='formTitle center non-alt'>
                        <h1>Hello travelers. Where would you like to go ?</h1>
                    </div>
                    <main>           
                        <div className="contenedor-tareas center">
                            <FormTicket/>
                        </div>
                    </main>
                </div>
                    
                </div>
            </div>  
        </Fragment>
    );
}
 
export default Principal;
import React, { useContext } from 'react';
import TicketContext from '../../context/ticket/ticketContext';

const Sidebar = () => {
    //Usamos el context y extraemmos tickets del StateInicial
   const ticketContext=useContext(TicketContext);
   const {cambiaBusqueda,obtenerTickets}=ticketContext;
    
    //funcion para agregar el proyecto actual
    const BusquedaNew = ()=>{
        let statusB= 'NEW';
        cambiaBusqueda(statusB);
        obtenerTickets();
    }
    const BusquedaReserved = ()=>{
        let statusB= 'RESERVED';
        cambiaBusqueda(statusB);
        obtenerTickets();
    }
    return ( 
        <aside>
            <h1>SUPERVISOR<span>TASKS</span></h1>
            <div className="proyectos">
                <h2>Request Lists</h2>
                <button
                            type="button"
                            className="btn btn-primario btn-block "
                            onClick={()=>BusquedaNew()}
                        >NEWS</button>
                        <br/>
                <button
                            type="button"
                            className="btn btn-primario btn-block"
                            onClick={()=>BusquedaReserved()}
                        >RESERVED</button>
            
            </div>
        </aside>
     );
}
 
export default Sidebar;
import React from 'react';
import {BrowserRouter as Router,Switch, Route} from 'react-router-dom';
import Login from './components/auth/Login';
import Tickets from './components/ticket/Tickets';


import TicketState from './context/ticket/ticketState';
import AlertaState from './context/alertas/alertaState';
import AuthState from './context/autenticacion/authState';
import Principal from './components/ticket/Principal';
import tokenAuth from './config/tokenAuth';
import RutaPrivada from './components/rutas/RutaPrivada';
//Revisar si tenemos un token
const token = localStorage.getItem('token');
if(token){
  tokenAuth(token);
}

function App() {
  return (
    <TicketState>
        <AlertaState>
          <AuthState>
            <Router>
              <Switch>
                <Route exact path="/" component={Principal}/>
                <Route exact path="/login" component={Login}/>
                <RutaPrivada exact path="/tikets" component={Tickets}/>
              </Switch>
            </Router>
          </AuthState>
        </AlertaState> 
    </TicketState>
  );
}

export default App;

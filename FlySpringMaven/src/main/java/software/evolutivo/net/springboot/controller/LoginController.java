package software.evolutivo.net.springboot.controller;


import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import software.evolutivo.net.springboot.exception.ResourceNotFoundException;
import software.evolutivo.net.springboot.model.Ticket;
import software.evolutivo.net.springboot.model.User;
import software.evolutivo.net.springboot.repository.LoginRepository;




/**
 * @author WINNER
 *
 */
@RestController
@RequestMapping("/RestLogin/")
public class LoginController {
	@Autowired
	private LoginRepository loginRepository;
	//get users
	@CrossOrigin
	@PostMapping("/login")//login
	public ResponseEntity<User> createTicket(
			@Validated  @RequestBody User userParam) throws ResourceNotFoundException {
		User userFind  = loginRepository.findUserByEmail(userParam.getEmail())
		.orElseThrow(()-> new ResourceNotFoundException("User does not exist ::" + userParam.getEmail()));
		if(!userFind.getPassword().equals(userParam.getPassword())) throw new ResourceNotFoundException("Password incorrect ::" + userParam.getEmail());
			 
		return ResponseEntity.ok(userFind);
	}
	//get user
	@CrossOrigin
	@GetMapping("/login/{id}")
	public ResponseEntity<User> getTicketById(@PathVariable(value="id")Long id) 
			throws ResourceNotFoundException {
		User user = loginRepository.findById(id)
				.orElseThrow(()->new ResourceNotFoundException("User id ::" + id));
		return ResponseEntity.ok().body(user);
	}
}

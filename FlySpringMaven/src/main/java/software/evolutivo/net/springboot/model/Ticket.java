/**
 * 
 */
package software.evolutivo.net.springboot.model;

import java.sql.Date;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
//import javax.persistence.Temporal;
//import javax.persistence.TemporalType;



/**
 * @author WINNER
 *
 */
@Entity
@Table(name="ticket")
public class Ticket {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;
	
	@Column(name="applicant")
	private String applicant;
	
	@Column(name="destination")
	private String destination;
	
	@Column(name="status")
	private String status;

	@Column(name="travelDate")
	//@Temporal(TemporalType.TIMESTAMP)
	private Date travelDate;
	
	@Column(name="createTicket")
	//@Temporal(TemporalType.TIMESTAMP)
	private Timestamp createTicket;
	
	@Column(name="updateTicket")
	//@Temporal(TemporalType.TIMESTAMP)
	private Timestamp  updateTicket;
	
	public Ticket() {
		super();
	}
	
	public Ticket(String applicant,String destination,String status, Date travelDate) {
		super();
		this.applicant=applicant;
		this.destination=destination;
		this.status=status;
		this.travelDate =travelDate;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getApplicant() {
		return applicant;
	}

	public void setApplicant(String applicant) {
		this.applicant = applicant;
	}

	public String getDestination() {
		return destination;
	}

	public void setDestination(String destination) {
		this.destination = destination;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Date getTravelDate() {
		return travelDate;
	}

	public void setTravelDate(Date travelDate) {
		this.travelDate = travelDate;
	}

	public Timestamp getCreateTicket() {
		return createTicket;
	}

	public void setCreateTicket(Timestamp createTicket) {
		this.createTicket = createTicket;
	}

	public Timestamp getUpdateTicket() {
		return updateTicket;
	}

	public void setUpdateTicket(Timestamp updateTicket) {
		this.updateTicket = updateTicket;
	}

	
	
	
}

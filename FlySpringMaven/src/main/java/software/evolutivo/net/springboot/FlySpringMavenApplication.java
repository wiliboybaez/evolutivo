package software.evolutivo.net.springboot;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FlySpringMavenApplication {

	public static void main(String[] args) {
		SpringApplication.run(FlySpringMavenApplication.class, args);
	}

}

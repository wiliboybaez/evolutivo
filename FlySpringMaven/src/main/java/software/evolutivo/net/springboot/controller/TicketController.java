/**
 * 
 */
package software.evolutivo.net.springboot.controller;

import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import software.evolutivo.net.springboot.exception.ResourceNotFoundException;
import software.evolutivo.net.springboot.model.Ticket;
import software.evolutivo.net.springboot.repository.TicketRepository;


/**
 * @author WINNER
 *
 */
@RestController
@RequestMapping("/RestTicket/")
public class TicketController {
	@Autowired
	private TicketRepository ticketRepository;
	//get tickets
	@CrossOrigin
	@GetMapping("/ticket")
	public List<Ticket> getAllTicket(){
		return this.ticketRepository.findAllByOrderByCreateTicketAsc();
	}
	//get ticket by id
	@CrossOrigin
	@GetMapping("/ticket/{id}")
	public ResponseEntity<Ticket> getTicketById(@PathVariable(value="id")Long ticketId) 
			throws ResourceNotFoundException {
		Ticket ticket = ticketRepository.findById(ticketId)
				.orElseThrow(()->new ResourceNotFoundException("Ticket no encontrada con es id ::" + ticketId));
		return ResponseEntity.ok().body(ticket);
		
	}
	//save ticket
	@CrossOrigin
	@PostMapping("/ticket")
	public Ticket createTicket(@RequestBody Ticket ticket) {
			LocalDateTime localDateTime = LocalDateTime.now();	
			Timestamp fechaActual=Timestamp.valueOf(localDateTime);
			ticket.setCreateTicket(fechaActual);
			return this.ticketRepository.save(ticket);
	}
	//update ticket
	@CrossOrigin
	@PostMapping("/ticket/{id}")
	public ResponseEntity<Ticket> updateTicket(@PathVariable(value = "id")Long ticketId,
			@Validated  @RequestBody Ticket ticketDetails) throws ResourceNotFoundException {
			LocalDateTime localDateTime = LocalDateTime.now();	
			Timestamp fechaActual=Timestamp.valueOf(localDateTime); 
			
			Ticket ticket = ticketRepository.findById(ticketId)
					.orElseThrow(()-> new ResourceNotFoundException("Ticket no encontrada con ese id ::" + ticketId));
			//ticket.setApplicant(ticketDetails.getApplicant());
			ticket.setUpdateTicket(fechaActual);
			ticket.setStatus(ticketDetails.getStatus());
			
			return ResponseEntity.ok(this.ticketRepository.save(ticket));
	}
	//delete ticket
	@CrossOrigin
	@DeleteMapping("/ticket/{id}")
	public Map<String, Boolean> deleteTicket(@PathVariable(value = "id")Long ticketId)
			throws ResourceNotFoundException {
		Ticket ticket = ticketRepository.findById(ticketId)
				.orElseThrow(()-> new ResourceNotFoundException("Ticket no encontrada con ese id ::" + ticketId));
		this.ticketRepository.delete(ticket);
		Map<String,Boolean> response = new HashMap<>();
		response.put("deleted", Boolean.TRUE);
		return response;
	}

}

package software.evolutivo.net.springboot.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import software.evolutivo.net.springboot.model.Ticket; 

/**
 * @author WINNER
 *
 */
@Repository
public interface TicketRepository extends JpaRepository<Ticket, Long> {
	public List<Ticket> findAllByOrderByCreateTicketAsc();
}


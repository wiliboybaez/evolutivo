package software.evolutivo.net.springboot.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import software.evolutivo.net.springboot.model.User;

/**
 * @author WINNER
 *
 */
@Repository
public interface LoginRepository extends JpaRepository<User, Long> {
	Optional<User> findUserByEmail(String email);
}


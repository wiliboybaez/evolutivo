# APLICACION RESERVA DE VUELOS
Este proyecto contiene dos partes el BACKEND Y FRONT-END

# 1. BACKEND  (FlySpringMaven)
	Necesita configura la BDD antes de ejecutar el Rest API.
## Base de datos
* Crear la base de datos con el nombre "flight", 
* Esta bdd contiene dos tablas: ticket y usuario (Usar el script flight.sql)
* Necesita crear un usuario de prueba: Email (supervisor@softwareevolutivo.net) Password (123)

## Rest API
Este Rest API se encuentra realizado en el IDE Eclipse(Spring Tool Suite) con Java 8 y Spring Boot, PostgresSQL,JPA,Hibernate con Maven. 
Contiene un CRUD con la tabla de "ticket" y auntenticación del usuario SUPERVISOR con la tabla "usuario".

* Configurar las variables de conexion a la Base de datos.
	* 1. Verificar el archivo FlySpringMaven\src\main\resources\application.properties
		spring.datasource.url=jdbc:postgresql://localhost:5432/flight (Verifica la ruta de conexion:puerto/'NombreBDD')
		spring.datasource.username=postgres (Usuario de la Bdd)
		spring.datasource.password=root (Password de la Bdd)
Ejecutar la aplicación en modo de desarrollo.<br>
[http://localhost:8080](http://localhost:8080) Ruta para ver en el navegador.


# 2. FRONT-END (flightclient)

Este proyecto esta realizado con REACT Incluye Hooks, Context.
La aplicación Web consume los servicios del RestApi en tres interfaces 
	-Publica: Creación de Solicitudes
	-Supervisor: Administra Solicitudes
	
* Configurar las variables de conexion al Rest API.
	* 1. Verificar el archivo flightclient\.env.development.local
		REACT_APP_BACKEND_URL=http://localhost:8080
- Credenciales default para iniciar sesión: Email (supervisor@softwareevolutivo.net) Password (123)

## Iniciar Proyecto en consola

### `npm install`

### `npm start`

Ejecutar la aplicación en modo de desarrollo.<br>
[http://localhost:3000](http://localhost:3000) Ruta para ver en el navegador.


